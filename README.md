# README #

### Personalization document factory ###

A simple project that marks a pdf with a personal message on every page. This could be to stop piracy after selling a pdf or as a personal thank you message after crowdfunding.

### How do I get set up? ###

* Clone latest develop or master
* Build using maven 'mvn clean package'
* In the target folder, use the 'personalize-pdf.on-jar.jar'
* Run the application with the 'personalize.cfg' in the current directory

### Who do I talk to? ###

* Markus Kruse
* http://www.rollspel.nu/forum/allm%C3%A4nna-diskussionsforum/off-topic/35366-prorgram-f%C3%B6r-att-personalisera-en-pdf (in swedish)