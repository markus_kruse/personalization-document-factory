package kruse.pdfpers;

import com.itextpdf.text.Font;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: markus
 * Date: 2014-07-19
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
public class PersonalizationContext {

    private File inputFile;
    private String message;
    private String[] messageParameters;
    private float xMMFromLeft = -1;
    private float xMMFromRight = -1;
    private float yMMFromTop = -1;
    private float yMMFromBottom = -1;
    private Font font;

    public PersonalizationContext(File inputFile, String message, String[] messageParameters, float xMMFromLeft, float xMMFromRight, float yMMFromTop, float yMMFromBottom, Font font) {
        this.inputFile = inputFile;
        this.message = message;
        this.messageParameters = messageParameters;
        this.xMMFromLeft = xMMFromLeft;
        this.xMMFromRight = xMMFromRight;
        this.yMMFromTop = yMMFromTop;
        this.yMMFromBottom = yMMFromBottom;
        this.font = font;
    }

    public File getInputFile() {
        return inputFile;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getMessageParameters() {
        return messageParameters;
    }

    public void setMessageParameters(String[] messageParameters) {
        this.messageParameters = messageParameters;
    }

    public float getxMMFromLeft() {
        return xMMFromLeft;
    }

    public void setxMMFromLeft(float xMMFromLeft) {
        this.xMMFromLeft = xMMFromLeft;
    }

    public float getxMMFromRight() {
        return xMMFromRight;
    }

    public void setxMMFromRight(float xMMFromRight) {
        this.xMMFromRight = xMMFromRight;
    }

    public float getyMMFromTop() {
        return yMMFromTop;
    }

    public void setyMMFromTop(float yMMFromTop) {
        this.yMMFromTop = yMMFromTop;
    }

    public float getyMMFromBottom() {
        return yMMFromBottom;
    }

    public void setyMMFromBottom(float yMMFromBottom) {
        this.yMMFromBottom = yMMFromBottom;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }
}
