package kruse.pdfpers;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Created with IntelliJ IDEA.
 * User: markus
 * Date: 2014-07-17
 * Time: 21:40
 * To change this template use File | Settings | File Templates.
 */

public class PdfPersonalizer {

    public static void main(String[] args) {
        try {
            FileInputStream configFileInputStream = new FileInputStream("personalize.cfg");
            InputStreamReader configFileReader = new InputStreamReader(configFileInputStream, "UTF-8");
            Scanner scanner = new Scanner(configFileReader);
            Map<String, String> properties = new HashMap<String, String>();
            readConfigFile(scanner, properties);
            configFileReader.close();

            File inputFile = new File(properties.get("input-pdf"));
            if (!inputFile.exists() || !inputFile.canRead()) {
                throw new RuntimeException("Can't read file: " + inputFile.getName());
            }

            String message = properties.get("message");
            if (message == null) {
                throw new RuntimeException("Message is empty");
            }
            String params = properties.get("names");
            if (params == null) {
                throw new RuntimeException("Names is empty");
            }
            String nameParams[] = params.split(",");
            for (int i = 0; i < nameParams.length; i++) {
                nameParams[i] = nameParams[i].trim();
            }

            Font font;
            String typefaceFile = properties.get("typefacefile");
            String typeface = properties.get("typeface");
            float fontSize = Float.parseFloat(properties.get("fontsize"));
            if (typefaceFile != null) {
                FontFactory.register(typefaceFile, "my_font");
                font = FontFactory.getFont("my_font", fontSize);
            } else {
                font = FontFactory.getFont(typeface, fontSize);
            }


            PersonalizationContextBuilder builder = new PersonalizationContextBuilder()
                    .setInputFile(inputFile)
                    .setMessage(message)
                    .setFont(font)
                    .setMessageParameters(nameParams)
                    .setxMMFromLeft(Float.parseFloat(properties.get("fromleft")))
                    .setxMMFromRight(Float.parseFloat(properties.get("fromright")))
                    .setyMMFromBottom(Float.parseFloat(properties.get("fromtop")))
                    .setyMMFromBottom(Float.parseFloat(properties.get("fromtop")));

            doPersonalizeAll(builder.build());
        } catch (Throwable e) {
            System.out.println("Oups, it looks like something went wrong:");
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void readConfigFile(Scanner scanner, Map<String, String> properties) {
        properties.put("fromleft", "-1");
        properties.put("fromright", "-1");
        properties.put("frombottom", "-1");
        properties.put("fromtop", "-1");
        properties.put("fontsize", "11");
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] keyValuePair = line.split("=");
            String key = keyValuePair[0].trim();
            String value = keyValuePair[1].trim();
            properties.put(key, value);
        }
    }

    public static void doPersonalizeAll(PersonalizationContext context) {
        try {
            for (String messageParam : context.getMessageParameters()) {
                long time = System.currentTimeMillis();
                personalizeOne(context, messageParam);
                time = System.currentTimeMillis() - time;
                System.out.printf("Personalized pdf generated for %s in %s milliseconds\n", messageParam, time);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void personalizeOne(PersonalizationContext context, String messageParam) throws DocumentException, IOException {
        PdfReader originalPdfReader = new PdfReader(context.getInputFile().getAbsolutePath());
        int numberOfPages = originalPdfReader.getNumberOfPages();
        String personalFileName = messageParam.toLowerCase().replaceAll("[^a-zA-z]", "");
        PdfStamper markedPdfStamped = new PdfStamper(originalPdfReader, new FileOutputStream("result_" + personalFileName + ".pdf"));
        markedPdfStamped.setFullCompression();
        for (int page = 1; page <= numberOfPages; page++) {
            //gets pagesize for position
            Rectangle pageSize = originalPdfReader.getPageSize(page);

            //get canvas to draw over existing content
            PdfContentByte canvas;
            canvas = markedPdfStamped.getOverContent(page);

            //calculate position
            float x = context.getxMMFromLeft() < 0 ? pageSize.getWidth() - context.getxMMFromRight() : context.getxMMFromLeft();
            float y = context.getyMMFromBottom() < 0 ? pageSize.getHeight() - context.getyMMFromTop() : context.getyMMFromBottom();
            boolean fromRight = context.getxMMFromLeft() < 0;

            //check font
            boolean useSpecialFont = context.getFont() != null;

            //create text object
            ColumnText column = new ColumnText(canvas);
            column.setAlignment(fromRight ? Element.ALIGN_RIGHT : Element.ALIGN_LEFT);
            column.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
            column.setSimpleColumn(x, y, 500, 100);
            String personalMessage = String.format(context.getMessage(), messageParam);
            column.addElement(useSpecialFont ?
                    new Paragraph(personalMessage, context.getFont()) :
                    new Paragraph(personalMessage));
            column.go();
        }
        markedPdfStamped.close();
        originalPdfReader.close();
    }

}