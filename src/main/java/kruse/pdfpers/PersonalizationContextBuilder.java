package kruse.pdfpers;

import com.itextpdf.text.Font;

import java.io.File;

public class PersonalizationContextBuilder {
    private File inputFile;
    private String message;
    private String[] messageParameters;
    private float xMMFromLeft;
    private float xMMFromRight;
    private float yMMFromTop;
    private float yMMFromBottom;
    private Font font;

    public PersonalizationContextBuilder setInputFile(File inputFile) {
        this.inputFile = inputFile;
        return this;
    }

    public PersonalizationContextBuilder setMessage(String message) {
        this.message = message;
        return this;
    }

    public PersonalizationContextBuilder setMessageParameters(String[] messageParameters) {
        this.messageParameters = messageParameters;
        return this;
    }

    public PersonalizationContextBuilder setxMMFromLeft(float xMMFromLeft) {
        this.xMMFromLeft = xMMFromLeft;
        return this;
    }

    public PersonalizationContextBuilder setxMMFromRight(float xMMFromRight) {
        this.xMMFromRight = xMMFromRight;
        return this;
    }

    public PersonalizationContextBuilder setyMMFromTop(float yMMFromTop) {
        this.yMMFromTop = yMMFromTop;
        return this;
    }

    public PersonalizationContextBuilder setyMMFromBottom(float yMMFromBottom) {
        this.yMMFromBottom = yMMFromBottom;
        return this;
    }

    public PersonalizationContextBuilder setFont(Font font) {
        this.font = font;
        return this;
    }

    public PersonalizationContext build() {
        return new PersonalizationContext(inputFile, message, messageParameters, xMMFromLeft, xMMFromRight, yMMFromTop, yMMFromBottom, font);
    }
}